
> 21 base pair sequence with contains 4 ambiguities
  UCAUGAUCUCGUAAGATSRWY  

> Re-write 1
  UCAUGAUCUCGUAAGATGATC

> Re-write 2
  UCAUGAUCUCGUAAGATCATC 
 
> Re-write 3
  UCAUGAUCUCGUAAGATGGTC

> Reverse            --Number 2--
  CTACTAGAAUGCUCUAGUACU
 
> Complement         --Number 2--
  UGTUCTUGUGCUTTCTAGTAG

> Reverse-complement --Number 2--
  GATGATCTTUCGUGUTCUTGU

> Amino-acid chain   --Number 2--
  SStopSRKII

 
  
  